const mongoose = require('mongoose');

var StudentSchema = new mongoose.Schema({
  name : {
    type      : String,
    required  : true,
    trim      : true
  },
  email     : {
    type      : String,
    trim      : true
  }
});

var Student = mongoose.model('Student', StudentSchema);

module.exports = { Student };