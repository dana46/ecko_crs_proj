const mongoose = require('mongoose');

var CourseSchema = new mongoose.Schema({
  course_name : {
    type      : String,
    required  : true,
    trim      : true
  },
  tagline     : {
    type      : String,
    trim      : true
  },
  instructor  : {
    name : {
      type    : String,
      required: true
    },
    desc : {
      type    : String,
      requires: true,
      trim      : true
    }
  },
  course_duration : {
    type      : String,
    required  : true
  },
  is_free     : {
    type      : Boolean,
    required  : true
  },
  course_fee  : {
    type      : Number
  },
  includes    : [{
    type      : String,
    trim      : true
  }],
  learnings   : [{
    type      : String,
    trim      : true
  }],
  course_description : {
    type      : String,
    trim      : true
  },
  curriculum  : [{
    sub_heading : String,
    content     : String
  }],
  img_url     : {
    type      : String,
    trim      : true
  },
  _enrollments : [{
    type : mongoose.Schema.ObjectId,
    ref : 'Student' 
  }],
  created_at  : {
    type      : Date,
    default   : new Date()
  }
});

var Course = mongoose.model('Course', CourseSchema);

module.exports = { Course };