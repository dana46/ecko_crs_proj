const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true }) // connecting to DB via URI
module.exports = { mongoose }