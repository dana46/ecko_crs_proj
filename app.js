require('./config/config');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const _ = require('lodash');
const nodemailer = require('nodemailer');
//const secrets = require('./config/secrets.json');


app.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                          // parse application/json

app.set('port', process.env.PORT);

app.set('views', __dirname + '/views');
app.use('/required', express.static(__dirname + '/required'));
app.set('view engine', 'ejs');


var { mongoose } = require('./db/mongoose');         // load the db config
var { Course } = require('./models/Course');
var { Student } = require('./models/Student');


/**
 * Create Course
 */
app.post('/create/course', (req, res) => {
  console.log('Inside');
  var body = _.pick(req.body, ['course_name', 'tagline', 'instructor_name', 'instructor_desc', 'course_duration', 'is_free', 'includes', 'learnings', 'course_description', 'curriculum', 'img_url']);
  var courseDetails = {
    course_name : body.course_name,
    tagline     : body.tagline,
    instructor  : {
      name : body.instructor_name,
      desc : body.instructor_desc
    },
    course_duration : body.course_duration,
    is_free : body.is_free,
    includes : body.includes,
    learnings : body.learnings,
    description : body.course_description,
    img_url : body.img_url
  };
  console.log(`${JSON.stringify(courseDetails)}`)
  var course = new Course(courseDetails);
  course.save().then((course) => {
    Course.findOneAndUpdate({
      _id : course._id
    },{
      $push : {
        curriculum : {$each : body.curriculum}
      }
    },{
      upsert : true,
      new : true
    }).then((crs) => {
      res.status(200).send(crs);
    }).catch((e) => {
      res.status(400).send(e);
    });
  }).catch((e) => {
    res.status(400).send(e);
  })
});


/**
 * Fetch Courses
 */
app.get('/courses', (req, res) => {
  Course.find({}).sort({"created_at" : -1}).then((courses) => {
    res.render('pages/courses', {
      courses : courses
    });
  }).catch((e) => {
    console.log(e);
  });
});


/**
 * Fetch Course By ID
 */
app.get('/course/:id', (req, res) => {
  Course.findById({
    _id : req.params.id
  }).then((course) => {
    res.render('pages/home', {
      course : course
    })
  }).catch((e) => {
    console.log(e);
  })
});


/**
 * Enroll a Student in a Course
 */
app.post('/enroll/student', (req, res) => {
  console.log('Inside Enroll');
  var body = _.pick(req.body, ['name', 'email', 'crs_id']);
  var studentDetails = {
    name : body.name,
    email : body.email
  };
  console.log(JSON.stringify(studentDetails));
  var stud = new Student(studentDetails);
  stud.save().then((student) => {
    console.log('Inside save Student');
    Course.findOneAndUpdate({
      _id : body.crs_id
    }, {
      $addToSet :{
        _enrollments : student._id
      }
    }, {
      upset : true
    }).then((crs) => {
      console.log('Inside enroolling Student');
      let mailTransport = nodemailer.createTransport({
        host    : secrets.host,
        port    : secrets.port,
        secure  : secrets.secure,
        auth    : {
          user  : secrets.from,
          pass  : secrets.pass
        }
      });
      // creating mail options
      let mailOptions = {
        to      : student.email,
        cc      : secrets.cc,
        bcc     : secrets.bcc,
        subject : secrets.subject.replace("@course_name", crs.course_name),
        text    : secrets.text,
        html    : secrets.html.replace("@name", student.name).replace("@course_name", crs.course_name)
                  .replace("@code_group", "1234").replace("@instr_name", crs.instructor.name),
        attachments : secrets.attachments,
        priority    : secrets.priority
      }
      // sending mail with callback using transporter sendMail
      mailTransport.sendMail(mailOptions, (err, info) => {
        if(err) {
          // error response
          res.status(404).send(err);
        } else {
          // success response
          res.status(200).send('Successfully Enrolled!');
        }
      })
      //res.status(200).send();
    }).catch((e) => {
      console.log(e);
      res.status(404).send('Student not to Course!');
    })
  }).catch((e) => {
    console.log(e);
    res.status(404).send('Student not added!')
  })
});


/**
 * Fetch the course history
 */
app.get('/courses/list', (req, res) => {
  Course.find({})
  .populate('_enrollments')
  .then((courses) => {
    res.status(200).send(courses);
  }).catch((e) => {
    res.status(400).send(e);
  });
});


// root path
app.get('/', (req, res) => {
  res.redirect('/courses');
});


app.listen(app.get('port'), function () {
  console.log(`Running on port ${app.get('port')}!`);
});