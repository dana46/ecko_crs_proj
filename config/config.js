const config = require('./config.json');
let env = process.env.NODE_ENV || "dev";
var envConfig = config[env];

Object.keys(envConfig).forEach((key) => {
  process.env[key] = envConfig[key];
});

// console.log(`PROCESS ENVS => ${JSON.stringify(process.env)}`);