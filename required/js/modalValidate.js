$(document).ready(function() {
  $('#submit').click(function(){
    var name = $('#input_name').val();
    var emailId = $('#input_email').val();
    var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    if(!name || name == '') {
      $('#error_name').text('Kindly give name!').css('display', 'block');
      return false;
    } else if(!emailId || emailId == '') {
      $('#error_email').text('Kindly give email id!').css('display', 'block');
      return false;
    }/*  else if(emailId) {
      if(!pattern.match(emailId)){
        $('#error_email').text('Kindly give valid email id!').css('display', 'block');
        return false;
      }​
    } */ else {
      enrollStudent(name, emailId);
    }
  });

  $('#input_name').keypress(function() {
    $('#error_name').css('display', 'none');
  });

  $('#input_email').keypress(function() {
    $('#error_email').css('display', 'none');
  });
});

function enrollStudent(name, emailId) {
  var body = {
    name : name,
    email : emailId,
    crs_id : $('#crs_id').text()
  }
  $.ajax({
    url: '/enroll/student',
    type: 'POST',
    data: JSON.stringify(body),
    dataType: "json",
    contentType: 'application/json; charset=UTF-8',
    success: function(response) {
      console.log(response.responseText);
      $('#enrollModal').modal('hide');
      alert('Enrolled Successfully!');
    },
    error: function(e) {
        //console.log(e);
        //console.log(response.responseText);
      $('#enrollModal').modal('hide');
      alert('Enrolled Successfully!');
    }
});
}